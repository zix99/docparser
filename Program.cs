using System;
using docparse.Descriptors;
using docparse.Formats;
using System.IO;
using docparse.Parser;

namespace docparse
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            var opts = new Arguments (args);
            if (opts.ShowHelp || opts.Files.Length == 0)
            {
                opts.WriteHelp (Console.Out);
                return;
            }

            var formatter = FormatFactory.GetFormatter (opts.Format ?? string.Empty);

            TextWriter writer = Console.Out;
            try
            {
                if (!string.IsNullOrWhiteSpace(opts.Output) && opts.Output != "-")
                {
                    writer = new StreamWriter(File.OpenWrite (opts.Output));
                }
            }
            catch(Exception e)
            {
                Console.Error.WriteLine ("Failed to open output file: {0}", e.Message);
                Environment.Exit (1);
            }

            var parser = new docparse.Parser.DocumentParser ();
            parser.IgnoreString = opts.Ignore;
            parser.Namespaces.AddRange (opts.Namespaces);

            //Load up symbols
            DocumentSymbolDecorator decorator = null;
            if (opts.Symbols.Count > 0)
            {
                decorator = new DocumentSymbolDecorator ();
                foreach(var symbol in opts.Symbols)
                {
                    decorator.AddAssembly (symbol);
                }
            }

            bool hadError = false;
            foreach(var docfile in opts.Files)
            {
                Console.Error.WriteLine ("Parsing {0}...", docfile);
                try
                {
                    var docs = parser.Load(docfile);
                    if (decorator != null)
                        decorator.DecorateDocument(docs);
                    formatter.Generate(writer, docs);
                }
                catch(Exception e)
                {
                    Console.Error.WriteLine (e);
                    hadError = true;
                }
            }

            writer.Dispose();
            if (hadError)
                Environment.Exit (1);
        }
    }
}
