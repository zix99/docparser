using System;
using Mono.Options;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace docparse
{
    public class Arguments
    {
        public bool ShowHelp{get; private set;}

        public string Format{get; private set;}

        public string[] Files{ get; private set;}

        public string Output{get; private set;}

        public List<string> Namespaces{get; private set;}

        public string Ignore{ get; private set;}

        public List<string> Symbols{ get; private set;}

        public Arguments (IEnumerable<string> args)
        {
            this.Output = "out";
            this.Ignore = "UNDOC";
            this.Namespaces = new List<string> ();
            this.Symbols = new List<string> ();

            var options = CreateOptionSet ();
            this.Files = options.Parse(args).ToArray();
        }

        public void WriteHelp(TextWriter o)
        {
            o.WriteLine("docparse.exe: [arguments] <docs.xml>");
            o.WriteLine ("Version: {0}", Assembly.GetEntryAssembly().GetName().Version.ToString());
            CreateOptionSet().WriteOptionDescriptions(o);
        }

        private OptionSet CreateOptionSet()
        {
            return new OptionSet {
                {"h|?|help", "Display help", v => ShowHelp = true},
                {"f|format=", "Pick output format: text, doku", v => Format = v},
                {"o|out=", "Output path or file", v => Output = v},
                {"ns=", "Namespace filter; Can have multiple", v => this.Namespaces.Add(v)},
                {"ignore=", "Ignore documentation starting with this summary string", v => this.Ignore = v},
                {"s|symbols=", "Add a symbols file to reflect upon", v => this.Symbols.Add(v)}
            };
        }
    }
}

