using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using docparse.Descriptors;

namespace docparse.Parser
{
    public class DocumentSymbolDecorator
    {
        private static readonly BindingFlags BINDING_FLAGS = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.IgnoreCase;

        private readonly List<Assembly> _assemblies = new List<Assembly>();

        public DocumentSymbolDecorator ()
        {
        }

        public bool AddAssembly(string path)
        {
            try
            {
                _assemblies.Add(Assembly.ReflectionOnlyLoadFrom(path));
            }
            catch
            {
                Console.Error.WriteLine ("Failed to load assembly: {0}", path);
                return false;
            }
            return true;
        }

        public void DecorateDocument(AssemblyDoc doc)
        {
            foreach(var type in doc.Types)
            {
                var reflType = GetReflectedType(type.Name.FullName);
                if (reflType != null)
                {
                    DecorateType (type, reflType);
                }
            }
        }

        private void DecorateType(TypeDoc doc, Type type)
        {
            //Members
            foreach(var method in doc.Members.Where(x => x.Name.Type == NameType.Method))
            {
                try
                {
                    var refl = type.GetMethod (method.Name.Name, BINDING_FLAGS);
                    if (refl != null)
                    {
                        method.Scope = GetScopeString (refl);
                        method.Return.TypeName = refl.ReturnType.Name;

                        foreach(var prm in refl.GetParameters())
                        {
                            var paramDoc = method.Params.FirstOrDefault(x => x.Name == prm.Name);
                            if (paramDoc != null)
                            {
                                paramDoc.TypeName = prm.ParameterType.Name;
                            }
                        }

                    }
                }catch{
                }
            }

            //Properties
            foreach(var prop in doc.Members.Where(x => x.Name.Type == NameType.Property))
            {
                try
                {
                    var refl = type.GetProperty (prop.Name.Name, BINDING_FLAGS);
                    if (refl != null)
                    {
                        prop.Scope = GetScopeString (refl.GetGetMethod());
                        prop.Return.TypeName = refl.PropertyType.Name;
                    }
                }catch{
                }
            }

            //Fields
            foreach(var field in doc.Members.Where(x => x.Name.Type == NameType.Field))
            {
                try
                {
                    var refl = type.GetField (field.Name.Name, BINDING_FLAGS);
                    if (refl != null)
                    {
                        field.Scope = refl.IsPublic ? "Public" : "Private";
                        field.Return.TypeName = refl.FieldType.Name;
                    }
                }catch{
                }
            }

            //Events
            foreach(var field in doc.Members.Where(x => x.Name.Type == NameType.Event))
            {
                try
                {
                    var refl = type.GetEvent (field.Name.Name, BINDING_FLAGS);
                    if (refl != null)
                    {
                        field.Scope = "Event";
                        field.Return.TypeName = refl.EventHandlerType.Name;
                    }
                }catch{
                }
            }

        }

        private Type GetReflectedType(string name)
        {
            foreach(var asm in _assemblies)
            {
                var type = asm.GetType(name, false, true);
                if (type != null)
                    return type;
            }
            return null;
        }

        private static string GetScopeString(MethodBase info)
        {
            string scope;
            if (info.IsPublic)
                scope = "Public";
            else
                scope = "Private";

            if (info.IsStatic)
                scope += " Static";

            if (info.IsVirtual)
                scope += " Virtual";

            return scope;
        }

    }
}

