using System;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using docparse.Descriptors;
using docparse.Extensions;

namespace docparse.Parser
{
    public class DocumentParser
    {
        public string IgnoreString = "";

        public List<string> Namespaces = new List<string> ();

        public DocumentParser ()
        {
        }

        public AssemblyDoc LoadXml(XElement xml)
        {
            var asm = xml.Element ("assembly");

            var doc = new AssemblyDoc{
                Name = asm.Element("name").Value.Trim()
            };

            //First pass, parse all members
            var all = new List<Tuple<NameDoc, XElement>> ();
            foreach(var member in xml.Element("members").Elements())
            {
                string name = member.Attribute ("name").Value;
                var nameDoc = new NameDoc (name);
                if (ValidNamespace(nameDoc.Namespace))
                    all.Add (new Tuple<NameDoc, XElement> (nameDoc, member));
            }

            //Second pass, collect all types
            foreach(var type in all.Where(x => x.Item1.Type == NameType.Type))
            {
                var typeDoc = ParseType (type.Item1, type.Item2);
                if (!ShouldIgnoreSummary(typeDoc.Summary))
                {
                    //Nested, find all members/properties/etc that go in this type
                    foreach(var member in all.Where(x => x.Item1.Namespace == type.Item1.FullName))
                    {
                        var memberDoc = ParseMember (member.Item1, member.Item2);
                        if (!ShouldIgnoreSummary (memberDoc.Summary))
                            typeDoc.Members.Add(memberDoc);
                    }

                    doc.Types.Add (typeDoc);
                }
            }

            return doc;
        }

        public AssemblyDoc LoadXml(string s)
        {
            var xml = XElement.Parse (s);
            return LoadXml(xml);
        }

        public AssemblyDoc Load(Stream stream)
        {
            using(var reader = new StreamReader(stream))
            {
                return LoadXml (reader.ReadToEnd ());
            }
        }

        public AssemblyDoc Load(string filename)
        {
            using(var stream = File.OpenRead(filename))
            {
                return Load (stream);
            }
        }


        private bool ValidNamespace(string ns)
        {
            if (this.Namespaces.Count == 0)
                return true;
            for (int i=0; i<this.Namespaces.Count; ++i)
            {
                if (ns.StartsWith (this.Namespaces [i]))
                    return true;
            }
            return false;
        }

        private bool ShouldIgnoreSummary(string summary)
        {
            if (string.IsNullOrEmpty (this.IgnoreString))
                return false;
            return summary.StartsWith (this.IgnoreString);
        }



        private static MemberDoc ParseMember(NameDoc name, XElement xml)
        {
            var member = new MemberDoc ();
            member.Name = name;
            member.Summary = xml.ElementValue ("summary") ?? string.Empty;
            member.Remarks = xml.ElementValue ("remarks") ?? string.Empty;

            foreach(var param in xml.Elements("param"))
            {
                member.Params.Add (new ParamDoc{
                    Name = param.AttributeValue ("name") ?? "?",
                    Summary = param.Value.Trim() ?? string.Empty
                });
            }

            var ret = xml.Element ("returns");
            member.Return = new ParamDoc ();
            if (ret != null)
            {
                member.Return.Name = "return";
                member.Return.Summary = ret.Value.Trim ();
            }

            foreach(var see in xml.Elements("see"))
            {
                member.SeeAlso.Add (new NameDoc (see.AttributeValue ("cref")));
            }

            return member;
        }

        private static TypeDoc ParseType (NameDoc name, XElement xml)
        {
            var type = new TypeDoc ();
            type.Name = name;
            type.Summary = xml.ElementValue ("summary") ?? string.Empty;
            type.Remarks = xml.ElementValue ("remarks") ?? string.Empty;
            foreach(var see in xml.Elements("see"))
            {
                type.SeeAlso.Add (new NameDoc (see.AttributeValue ("cref")));
            }
            return type;
        }

    }
}

