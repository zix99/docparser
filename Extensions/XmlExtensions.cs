using System;
using System.Xml.Linq;
using System.Text;

namespace docparse.Extensions
{
    public static class XmlExtensions
    {
        public static string ElementValue(this XElement xml, string name)
        {
            var ele = xml.Element (name);
            if (ele != null)
                return ele.Value.Trim ();
            return null;
        }

        public static string AttributeValue(this XElement xml, string name)
        {
            var attr = xml.Attribute (name);
            if (attr != null)
                return attr.Value.Trim();
            return null;
        }

        public static string ElementRaw(this XElement xml, string name)
        {
            var ele = xml.Element (name);
            if (ele != null)
                return ele.ToString().Trim ();
            return null;
        }

        public static string AttributeRaw(this XElement xml, string name)
        {
            var attr = xml.Attribute (name);
            if (attr != null)
                return attr.ToString().Trim();
            return null;
        }

        public static string ExtractMarkup(this XElement xml)
        {
            var sb = new StringBuilder ();
            foreach(var ele in xml.Elements())
            {
                sb.Append (ele.Value ?? string.Empty);
            }
            return sb.ToString ();
        }
    }
}

