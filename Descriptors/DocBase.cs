using System;
using System.Collections.Generic;

namespace docparse.Descriptors
{
    public class DocBase
    {
        public NameDoc Name{ get; set;}

        public string Summary{get; set;}

        public string Remarks{get; set;}

        public readonly List<NameDoc> SeeAlso = new List<NameDoc>();
    }
}

