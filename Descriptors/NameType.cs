using System;

namespace docparse.Descriptors
{
    public enum NameType
    {
        Undefined=0,
        Namespace,
        Type,
        Field,
        Property,
        Method,
        Event,
        Error
    }
}

