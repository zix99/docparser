using System;

namespace docparse.Descriptors
{
    public class ParamDoc
    {
        public string Name{get; set;}

        public string Summary{get; set;}

        public string TypeName{get;set;}
    }
}

