using System;
using System.Xml.Linq;
using System.Collections.Generic;
using docparse.Extensions;

namespace docparse.Descriptors
{
    public class TypeDoc : DocBase
    {
        public readonly List<MemberDoc> Members = new List<MemberDoc>();
    }
}

