using System;
using System.Linq;

namespace docparse.Descriptors
{
    public class NameDoc
    {
        public NameType Type{get; protected set;}

        public string Namespace{get; protected set;}

        public string Name{ get; protected set;}

        public string[] Params{ get; protected set;}

        public string FullName{get; protected set;}

        public NameDoc (string encodedName)
        {
            this.Type = GetTypeFromSymbol (encodedName [0]);

            string fullDesc = encodedName.Substring (2);
            if (this.Type == NameType.Namespace)
            {
                this.Name = this.Namespace = this.FullName = fullDesc;
            }
            else if(this.Type == NameType.Method)
            {
                int argStart = fullDesc.IndexOf ("(");
                string methodName, args;
                if (argStart < 0)
                {
                    methodName = fullDesc;
                    args = "";
                }
                else
                {
                    methodName = fullDesc.Substring (0, argStart);
                    args = fullDesc.Substring (argStart + 1, fullDesc.Length - argStart - 1);
                }

                //Name
                string[] parts = methodName.Split ('.');
                this.Namespace = string.Join (".", parts.Take (parts.Length - 1));
                this.Name = parts [parts.Length - 1];
                this.FullName = methodName;

                //Params
                this.Params = args.Split (',');
            }
            else if(this.Type == NameType.Undefined)
            {
                this.Name = encodedName;
                this.FullName = encodedName;
            }
            else
            {
                string[] parts = fullDesc.Split ('.');
                this.Namespace = string.Join (".", parts.Take (parts.Length - 1));
                this.Name = parts [parts.Length - 1];
                this.FullName = fullDesc;
            }

        }

        private static NameType GetTypeFromSymbol(char s)
        {
            switch(s)
            {
            case 'N':
                return NameType.Namespace;
            case 'T':
                return NameType.Type;
            case 'F':
                return NameType.Field;
            case 'P':
                return NameType.Property;
            case 'M':
                return NameType.Method;
            case 'E':
                return NameType.Event;
            case '!':
                return NameType.Error;
            }
            return NameType.Undefined;
        }

        public override string ToString ()
        {
            return string.Format ("{0}: {1}.{2}", this.Type.ToString(), this.Namespace, this.Name);
        }
    }
}

