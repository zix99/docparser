using System;
using System.Xml.Linq;
using docparse.Extensions;
using System.Collections.Generic;

namespace docparse.Descriptors
{
    public class MemberDoc : DocBase
    {
        public readonly List<ParamDoc> Params = new List<ParamDoc> ();

        public ParamDoc Return{ get; set;}

        public string Scope{get;set;}
    }
}

