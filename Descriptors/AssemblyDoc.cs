using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;

namespace docparse.Descriptors
{
    public class AssemblyDoc
    {
        public string Name{ get; set;}

        public readonly List<TypeDoc> Types = new List<TypeDoc> ();
    }
}

