using System;

namespace docparse.Formats
{
    public static class FormatFactory
    {
        public static IFormatter GetFormatter(string name)
        {
            switch(name.ToLowerInvariant())
            {
            case "doku":
                return new DokuFormatter ();
            case "text":
            default:
                return new BasicFormatter ();
            }
        }
    }
}

