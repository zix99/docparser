using System;
using System.IO;
using docparse.Descriptors;

namespace docparse.Formats
{
    public class BasicFormatter : IFormatter
    {
        public BasicFormatter ()
        {
        }

        #region IFormatter implementation

        public void Generate (TextWriter writer, AssemblyDoc asm)
        {
            writer.WriteLine ("Assembly: {0}", asm.Name);
            writer.WriteLine ("Types:");
            foreach(var type in asm.Types)
            {
                writer.WriteLine ("  {0}", type.Name.FullName);
                writer.WriteLine ("    Summary: {0}", type.Summary ?? string.Empty);
                foreach(var member in type.Members)
                {
                    writer.WriteLine ("   ->{0} {1} {2}: {3}", member.Scope, member.Return.TypeName, member.Name.Name, member.Summary);
                }
            }
        }

        #endregion
    }
}

