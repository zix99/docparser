using System;
using docparse.Descriptors;
using System.IO;

namespace docparse.Formats
{
    public interface IFormatter
    {
        void Generate(TextWriter writer, AssemblyDoc asm);
    }
}

